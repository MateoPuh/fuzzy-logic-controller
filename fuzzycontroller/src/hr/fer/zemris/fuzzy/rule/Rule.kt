package hr.fer.zemris.fuzzy.rule

import hr.fer.zemris.fuzzy.domain.DomainElement
import hr.fer.zemris.fuzzy.function.BinaryFunction
import hr.fer.zemris.fuzzy.set.FuzzySet
import hr.fer.zemris.fuzzy.set.MutableFuzzySet

class Rule(
    private val antecedents: Map<String, FuzzySet>,
    private val consequent: FuzzySet
) {

    fun calculateValue(values: Map<String, Int>, tNorm: BinaryFunction): FuzzySet {
        val result = values
            .entries
            .filter { antecedents.containsKey(it.key) }
            .map { antecedents[it.key]!!.getValueAt(DomainElement.of(it.value)) }
            .reduce(tNorm::valueAt)

        val conclusionSet = MutableFuzzySet(consequent.domain)
        for (e in conclusionSet.domain) {
            conclusionSet.set(e, tNorm.valueAt(consequent.getValueAt(e), result))
        }

        return conclusionSet
    }
}
