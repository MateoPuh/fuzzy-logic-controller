package hr.fer.zemris.fuzzy.rule

import hr.fer.zemris.fuzzy.function.BinaryFunction
import hr.fer.zemris.fuzzy.function.Operations
import hr.fer.zemris.fuzzy.set.FuzzySet

class RuleBase(
    private val rules: List<Rule>
) {

    fun calculate(values: Map<String, Int>, sNorm: BinaryFunction, tNorm: BinaryFunction): FuzzySet {
        return rules
            .map { it.calculateValue(values, tNorm) }
            .reduce { acc, d -> Operations.binaryOperation(acc, d, sNorm) }
    }
}
