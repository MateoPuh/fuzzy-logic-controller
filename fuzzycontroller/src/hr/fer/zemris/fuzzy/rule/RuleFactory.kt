package hr.fer.zemris.fuzzy.rule

import hr.fer.zemris.fuzzy.controller.sets.*

object RuleFactory {

    fun createKRuleBase() =
        RuleBase(
            listOf(
                turnSharpLeftRule(),
                turnSharpRightRule(),
                turnLeftCriticalRule(),
                turnRightCriticalRule(),
                changeDirectionRule()
            )
        )

    private fun turnSharpLeftRule() = Rule(
        mapOf(
            "dk" to blizuSet()
        ),
        ostroDesnoSet()
    )

    private fun turnSharpRightRule() = Rule(
        mapOf(
            "lk" to blizuSet()
        ),
        ostroLijevoSet()
    )

    private fun turnLeftCriticalRule() = Rule(
        mapOf(
            "dk" to kriticnoBlizuSet()
        ),
        ostroDesnoSet()
    )

    private fun turnRightCriticalRule() = Rule(
        mapOf(
            "lk" to kriticnoBlizuSet()
        ),
        ostroLijevoSet()
    )

    private fun changeDirectionRule() = Rule(
        mapOf(
            "s" to kriviSmjerSet()
        ),
        ostroLijevoSet()
    )

    fun createARuleBase() =
        RuleBase(
            listOf(
                speedUpRule(),
                slowDownRule()
            )
        )

    private fun speedUpRule() = Rule(
        mapOf(
            "v" to sporaBrzinaSet()
        ),
        ubrzavanjeSet()
    )

    private fun slowDownRule() = Rule(
        mapOf(
            "v" to brzaBrzinaSet()
        ),
        usporavanjeSet()
    )
}
