package hr.fer.zemris.fuzzy.system

import hr.fer.zemris.fuzzy.rule.RuleBase
import hr.fer.zemris.fuzzy.defuzzyfication.Defuzzifier
import hr.fer.zemris.fuzzy.function.Operations

open class ProductFuzzySystem(
    ruleBase: RuleBase,
    defuzzifier: Defuzzifier
): FuzzySystem(
    ruleBase,
    defuzzifier,
    Operations.zadehOr(),
    Operations.product()
)
