package hr.fer.zemris.fuzzy.system

import hr.fer.zemris.fuzzy.rule.RuleFactory
import hr.fer.zemris.fuzzy.defuzzyfication.Defuzzifier

class KMinimumFuzzySystem(
    defuzzifier: Defuzzifier
): MinimumFuzzySystem(
    RuleFactory.createKRuleBase(),
    defuzzifier
)
