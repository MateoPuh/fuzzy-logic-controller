package hr.fer.zemris.fuzzy.system

import hr.fer.zemris.fuzzy.controller.ControllerParameters
import hr.fer.zemris.fuzzy.rule.RuleBase
import hr.fer.zemris.fuzzy.defuzzyfication.Defuzzifier
import hr.fer.zemris.fuzzy.function.BinaryFunction

open class FuzzySystem(
    private val ruleBase: RuleBase,
    private val defuzzifier: Defuzzifier,
    private val sNorm: BinaryFunction,
    private val tNorm: BinaryFunction
) {

    fun calculate(input: ControllerParameters): Int {
        return defuzzifier.defuzzify(ruleBase.calculate(input.toMap(), sNorm, tNorm))
    }
}
