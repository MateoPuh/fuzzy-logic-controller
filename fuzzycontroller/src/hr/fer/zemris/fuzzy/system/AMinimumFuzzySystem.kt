package hr.fer.zemris.fuzzy.system

import hr.fer.zemris.fuzzy.rule.RuleFactory
import hr.fer.zemris.fuzzy.defuzzyfication.Defuzzifier

class AMinimumFuzzySystem(
    defuzzifier: Defuzzifier
): MinimumFuzzySystem(
    RuleFactory.createARuleBase(),
    defuzzifier
)
