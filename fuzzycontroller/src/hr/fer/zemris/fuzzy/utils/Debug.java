package hr.fer.zemris.fuzzy.utils;

import hr.fer.zemris.fuzzy.domain.Domain;
import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.set.FuzzySet;

public class Debug {

    public static void print(Domain domain, String headingText) {
        if (headingText != null) {
            System.out.println(headingText);
        }
        for (DomainElement e : domain) {
            System.out.println("Element domene: " + e);
        }
        System.out.println("Kardinalitet domene je: " + domain.getCardinality());
        System.out.println();
    }

    public static void print(FuzzySet fuzzySet, String headingText) {
        if (headingText != null) {
            System.out.println(headingText);
        }
        for (DomainElement d : fuzzySet.getDomain()) {
            System.out.println("d(" + d + ")=" + fuzzySet.getValueAt(d));
        }
        System.out.println();
    }
}
