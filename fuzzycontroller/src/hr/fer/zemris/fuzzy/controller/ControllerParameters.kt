package hr.fer.zemris.fuzzy.controller

data class ControllerParameters(
    val l: Int,
    val d: Int,
    val lk: Int,
    val dk: Int,
    val v: Int,
    val s: Int
) {

    companion object {

        fun from(line: String) =
            line.split(" ")
                .let {
                    ControllerParameters(
                        it[0].toInt(),
                        it[1].toInt(),
                        it[2].toInt(),
                        it[3].toInt(),
                        it[4].toInt(),
                        it[5].toInt()
                    )
                }
    }

    fun toMap() = mapOf<String, Int>(
        "l" to l,
        "d" to d,
        "lk" to lk,
        "dk" to dk,
        "v" to v,
        "s" to s
    )
}
