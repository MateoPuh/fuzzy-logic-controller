package hr.fer.zemris.fuzzy.controller

import hr.fer.zemris.fuzzy.defuzzyfication.COADefuzzifier
import hr.fer.zemris.fuzzy.defuzzyfication.Defuzzifier
import hr.fer.zemris.fuzzy.system.*

private const val KRAJ = "KRAJ"

fun main() {
    fun String.isEndOfStream() = this == KRAJ

    val def: Defuzzifier = COADefuzzifier()

    val fsAkcel: FuzzySystem = AProductFuzzySystem(def)
    val fsKormilo: FuzzySystem = KProductFuzzySystem(def)

    while (true) {
        val line = readLine()!!
        if (line.isEndOfStream()) {
            return
        }

        val params = ControllerParameters.from(line)

        val k = fsKormilo.calculate(params)
        val a = fsAkcel.calculate(params)

        val outputs = ControllerOutputs(k, a)

        println(outputs.toString())
        System.out.flush()
    }
}
