package hr.fer.zemris.fuzzy.controller.sets

import hr.fer.zemris.fuzzy.domain.DomainElement
import hr.fer.zemris.fuzzy.function.IntUnaryFunction
import hr.fer.zemris.fuzzy.set.CalculatedFuzzySet
import hr.fer.zemris.fuzzy.set.FuzzySet
import hr.fer.zemris.fuzzy.set.MutableFuzzySet
import hr.fer.zemris.fuzzy.set.StandardFuzzySets

private const val SB_ALPHA = 10
private const val SB_BETA = 30

private const val NB_ALPHA = 10
private const val NB_BETA = 20
private const val NB_GAMMA = 30

private const val BB_ALPHA = 25
private const val BB_BETA = 40

private const val US_ALPHA = -3
private const val US_BETA = -1

private const val JB_ALPHA = -2
private const val JB_BETA = 0
private const val JB_GAMMA = 2

private const val UB_ALPHA = 1
private const val UB_BETA = 3

private const val KB_ALPHA = 15
private const val KB_BETA = 35

private const val B_ALPHA = 30
private const val B_BETA = 50
private const val B_GAMMA = 70

private const val SU_ALPHA = 60
private const val SU_BETA = 90

private const val OL_ALPHA = -35
private const val OL_BETA = -25

private const val LL_ALPHA = -25
private const val LL_BETA = -15
private const val LL_GAMMA = -5

private const val R_ALPHA = -5
private const val R_BETA = 0
private const val R_GAMMA = 5

private const val LD_ALPHA = 5
private const val LD_BETA = 15
private const val LD_GAMMA = 25

private const val OD_ALPHA = 25
private const val OD_BETA = 35

fun dobarSmjerSet() =
    MutableFuzzySet(
        directionDomain()
    ).apply {
        set(DomainElement.of(0), 0.0)
        set(DomainElement.of(1), 1.0)
    }

fun kriviSmjerSet() =
    MutableFuzzySet(
        directionDomain()
    ).apply {
        set(DomainElement.of(0), 1.0)
        set(DomainElement.of(1), 0.0)
    }

fun sporaBrzinaSet() = sporaBrzina().let {
    CalculatedFuzzySet(
        velocityDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun normalnaBrzinaSet() = normalnaBrzina().let {
    CalculatedFuzzySet(
        velocityDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun brzaBrzinaSet() = brzaBrzina().let {
    CalculatedFuzzySet(
        velocityDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun usporavanjeSet() = uporavanje().let {
    CalculatedFuzzySet(
        accelerationDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun jednakaBrzinaSet() = jednakaBrzina().let {
    CalculatedFuzzySet(
        accelerationDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun ubrzavanjeSet() = ubrzavanje().let {
    CalculatedFuzzySet(
        accelerationDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun kriticnoBlizuSet() = kriticnoBlizu().let {
    CalculatedFuzzySet(
        distanceDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun blizuSet(): FuzzySet = blizu().let {
    CalculatedFuzzySet(
        distanceDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun sigurnaUdaljenostSet() = sigurnaUdaljenost().let {
    CalculatedFuzzySet(
        distanceDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun ostroLijevoSet() = ostroLijevo().let {
    CalculatedFuzzySet(
        angleDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun laganoLijevoSet() = laganoLijevo().let {
    CalculatedFuzzySet(
        angleDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun ravnoSet() = ravno().let {
    CalculatedFuzzySet(
        angleDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun laganoDesnoSet() = laganoDesno().let {
    CalculatedFuzzySet(
        angleDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun ostroDesnoSet() = ostroDesno().let {
    CalculatedFuzzySet(
        angleDomain(),
        IntUnaryFunction { index ->
            it.valueAt(index)
        }
    )
}

fun sporaBrzina() = StandardFuzzySets.lFunction(
    velocityDomain().indexOfElement(DomainElement.of(SB_ALPHA)),
    velocityDomain().indexOfElement(DomainElement.of(SB_BETA))
)

fun normalnaBrzina() = StandardFuzzySets.lambdaFunction(
    velocityDomain().indexOfElement(DomainElement.of(NB_ALPHA)),
    velocityDomain().indexOfElement(DomainElement.of(NB_BETA)),
    velocityDomain().indexOfElement(DomainElement.of(NB_GAMMA))
)

fun brzaBrzina() = StandardFuzzySets.gammaFunction(
    velocityDomain().indexOfElement(DomainElement.of(BB_ALPHA)),
    velocityDomain().indexOfElement(DomainElement.of(BB_BETA))
)

fun uporavanje() = StandardFuzzySets.lFunction(
    accelerationDomain().indexOfElement(DomainElement.of(US_ALPHA)),
    accelerationDomain().indexOfElement(DomainElement.of(US_BETA))
)

fun jednakaBrzina() = StandardFuzzySets.lambdaFunction(
    accelerationDomain().indexOfElement(DomainElement.of(JB_ALPHA)),
    accelerationDomain().indexOfElement(DomainElement.of(JB_BETA)),
    accelerationDomain().indexOfElement(DomainElement.of(JB_GAMMA))
)

fun ubrzavanje() = StandardFuzzySets.gammaFunction(
    accelerationDomain().indexOfElement(DomainElement.of(UB_ALPHA)),
    accelerationDomain().indexOfElement(DomainElement.of(UB_BETA))
)

fun kriticnoBlizu() = StandardFuzzySets.lFunction(
    directionDomain().indexOfElement(DomainElement.of(KB_ALPHA)),
    directionDomain().indexOfElement(DomainElement.of(KB_BETA))
)

fun blizu() = StandardFuzzySets.lambdaFunction(
    directionDomain().indexOfElement(DomainElement.of(B_ALPHA)),
    directionDomain().indexOfElement(DomainElement.of(B_BETA)),
    directionDomain().indexOfElement(DomainElement.of(B_GAMMA))
)

fun sigurnaUdaljenost() = StandardFuzzySets.gammaFunction(
    directionDomain().indexOfElement(DomainElement.of(SU_ALPHA)),
    directionDomain().indexOfElement(DomainElement.of(SU_BETA))
)

fun ostroLijevo() = StandardFuzzySets.lFunction(
    angleDomain().indexOfElement(DomainElement.of(OL_ALPHA)),
    angleDomain().indexOfElement(DomainElement.of(OL_BETA))
)

fun laganoLijevo() = StandardFuzzySets.lambdaFunction(
    angleDomain().indexOfElement(DomainElement.of(LL_ALPHA)),
    angleDomain().indexOfElement(DomainElement.of(LL_BETA)),
    angleDomain().indexOfElement(DomainElement.of(LL_GAMMA))
)

fun ravno() = StandardFuzzySets.lambdaFunction(
    angleDomain().indexOfElement(DomainElement.of(R_ALPHA)),
    angleDomain().indexOfElement(DomainElement.of(R_BETA)),
    angleDomain().indexOfElement(DomainElement.of(R_GAMMA))
)

fun laganoDesno() = StandardFuzzySets.lambdaFunction(
    angleDomain().indexOfElement(DomainElement.of(LD_ALPHA)),
    angleDomain().indexOfElement(DomainElement.of(LD_BETA)),
    angleDomain().indexOfElement(DomainElement.of(LD_GAMMA))
)

fun ostroDesno() = StandardFuzzySets.gammaFunction(
    angleDomain().indexOfElement(DomainElement.of(OD_ALPHA)),
    angleDomain().indexOfElement(DomainElement.of(OD_BETA))
)
