package hr.fer.zemris.fuzzy.controller.sets

import hr.fer.zemris.fuzzy.domain.DomainFactory

private const val DISTANCE_DOMAIN_START = 0
private const val DISTANCE_DOMAIN_END = 1301

private const val ANGLE_DOMAIN_START = -90
private const val ANGLE_DOMAIN_END = 91

private const val ACCELERATION_DOMAIN_START = -4
private const val ACCELERATION_DOMAIN_END = 5

private const val VELOCITY_DOMAIN_START = 0
private const val VELOCITY_DOMAIN_END = 81

private const val DIRECTION_DOMAIN_START = 0
private const val DIRECTION_DOMAIN_END = 2

fun distanceDomain() = DomainFactory.intRange(DISTANCE_DOMAIN_START, DISTANCE_DOMAIN_END)

fun angleDomain() = DomainFactory.intRange(ANGLE_DOMAIN_START, ANGLE_DOMAIN_END)

fun accelerationDomain() = DomainFactory.intRange(ACCELERATION_DOMAIN_START, ACCELERATION_DOMAIN_END)

fun velocityDomain() = DomainFactory.intRange(VELOCITY_DOMAIN_START, VELOCITY_DOMAIN_END)

fun directionDomain() = DomainFactory.intRange(DIRECTION_DOMAIN_START, DIRECTION_DOMAIN_END)
