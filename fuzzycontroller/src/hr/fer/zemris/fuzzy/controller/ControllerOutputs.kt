package hr.fer.zemris.fuzzy.controller

data class ControllerOutputs(
    val k: Int,
    val a: Int
) {

    override fun toString(): String {
        return "$a $k"
    }
}
