package hr.fer.zemris.fuzzy;

import hr.fer.zemris.fuzzy.domain.Domain;
import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.domain.DomainFactory;
import hr.fer.zemris.fuzzy.utils.Debug;

public class Domene {

    public static void main(String[] args) {
        Domain d1 = DomainFactory.intRange(0, 5); // {0,1,2,3,4}
        Debug.print(d1, "Elementi domene d1:");
        Domain d2 = DomainFactory.intRange(0, 3); // {0,1,2}
        Debug.print(d2, "Elementi domene d2:");
        Domain d3 = DomainFactory.combine(d1, d2);
        Debug.print(d3, "Elementi domene d3:");
        System.out.println(d3.elementForIndex(0));
        System.out.println(d3.elementForIndex(5));
        System.out.println(d3.elementForIndex(14));
        System.out.println(d3.indexOfElement(DomainElement.of(4, 1)));
    }
}