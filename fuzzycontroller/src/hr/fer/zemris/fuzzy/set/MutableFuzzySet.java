package hr.fer.zemris.fuzzy.set;

import hr.fer.zemris.fuzzy.domain.Domain;
import hr.fer.zemris.fuzzy.domain.DomainElement;

public class MutableFuzzySet implements FuzzySet {

    private final double[] memberships;
    private final Domain domain;

    public MutableFuzzySet(Domain domain) {
        this.domain = domain;
        this.memberships = new double[domain.getCardinality()];
    }

    @Override
    public Domain getDomain() {
        return domain;
    }

    @Override
    public double getValueAt(DomainElement element) {
        return memberships[domain.indexOfElement(element)];
    }

    public MutableFuzzySet set(DomainElement element, double value) {
        memberships[domain.indexOfElement(element)] = value;
        return this;
    }
}
