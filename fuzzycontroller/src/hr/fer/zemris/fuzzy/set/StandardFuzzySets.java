package hr.fer.zemris.fuzzy.set;

import hr.fer.zemris.fuzzy.function.IntUnaryFunction;

public class StandardFuzzySets {

    public static IntUnaryFunction lFunction(int alpha, int beta) {
        return new IntUnaryFunction() {
            @Override
            public double valueAt(int index) {
                if (index < alpha) {
                    return 1;
                }

                if (index >= beta) {
                    return 0;
                }

                return (double) (beta - index) / (beta - alpha);
            }
        };
    }

    public static IntUnaryFunction gammaFunction(int alpha, int beta) {
        return new IntUnaryFunction() {
            @Override
            public double valueAt(int index) {
                if (index < alpha) {
                    return 0;
                }

                if (index >= beta) {
                    return 1;
                }

                return (double) (index - alpha) / (beta - alpha);
            }
        };
    }

    public static IntUnaryFunction lambdaFunction(int alpha, int beta, int gamma) {
        return new IntUnaryFunction() {
            @Override
            public double valueAt(int index) {
                if (index < alpha || index >= gamma) {
                    return 0;
                }

                if (index < beta) {
                    return (double) (index - alpha) / (beta - alpha);
                } else {
                    return (double) (gamma - index) / (gamma - beta);
                }
            }
        };
    }
}
