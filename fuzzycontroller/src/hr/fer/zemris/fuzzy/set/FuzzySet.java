package hr.fer.zemris.fuzzy.set;

import hr.fer.zemris.fuzzy.domain.Domain;
import hr.fer.zemris.fuzzy.domain.DomainElement;

public interface FuzzySet {

    Domain getDomain();

    double getValueAt(DomainElement element);
}
