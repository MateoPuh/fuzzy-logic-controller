package hr.fer.zemris.fuzzy.set;

import hr.fer.zemris.fuzzy.domain.Domain;
import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.function.IntUnaryFunction;

public class CalculatedFuzzySet implements FuzzySet {

    private final Domain domain;
    private final IntUnaryFunction function;

    public CalculatedFuzzySet(Domain domain, IntUnaryFunction function) {
        this.domain = domain;
        this.function = function;
    }

    @Override
    public Domain getDomain() {
        return domain;
    }

    @Override
    public double getValueAt(DomainElement element) {
        return function.valueAt(domain.indexOfElement(element));
    }
}
