package hr.fer.zemris.fuzzy.function;

import hr.fer.zemris.fuzzy.set.CalculatedFuzzySet;
import hr.fer.zemris.fuzzy.set.FuzzySet;

public class Operations {

    public static FuzzySet unaryOperation(FuzzySet set, UnaryFunction function) {
        return new CalculatedFuzzySet(
                set.getDomain(),
                index -> function.valueAt(set.getValueAt(set.getDomain().elementForIndex(index)))
        );
    }

    public static FuzzySet binaryOperation(FuzzySet set1, FuzzySet set2, BinaryFunction function) {
        return new CalculatedFuzzySet(
                set1.getDomain(),
                index -> function.valueAt(
                        set1.getValueAt(set1.getDomain().elementForIndex(index)),
                        set2.getValueAt(set2.getDomain().elementForIndex(index)))
        );
    }

    public static UnaryFunction zadehNot() {
        return x -> 1 - x;
    }

    public static BinaryFunction zadehAnd() {
        return Math::min;
    }

    public static BinaryFunction product() {
        return (x, y) -> x * y;
    }

    public static BinaryFunction zadehOr() {
        return Math::max;
    }

    public static BinaryFunction hamacherTNorm(double theta) {
        return (x, y) -> (x * y) / (theta + (1 - theta) * (x + y - x * y));
    }

    public static BinaryFunction hamacherSNorm(double theta) {
        return (x, y) -> (x + y - (2 - theta) * x * y) / (1 - (1 - theta) * x * y);
    }
}
