package hr.fer.zemris.fuzzy.function;

public interface IntUnaryFunction {

    double valueAt(int index);
}
