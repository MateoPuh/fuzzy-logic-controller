package hr.fer.zemris.fuzzy.function;

public interface BinaryFunction {

    double valueAt(double x, double y);
}
