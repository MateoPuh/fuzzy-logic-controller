package hr.fer.zemris.fuzzy.function;

public interface UnaryFunction {

    double valueAt(double x);
}
