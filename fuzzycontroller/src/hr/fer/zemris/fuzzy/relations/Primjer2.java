package hr.fer.zemris.fuzzy.relations;

import hr.fer.zemris.fuzzy.domain.Domain;
import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.domain.DomainFactory;
import hr.fer.zemris.fuzzy.set.FuzzySet;
import hr.fer.zemris.fuzzy.set.MutableFuzzySet;

public class Primjer2 {

    public static void main(String[] args) {
        Domain u1 = DomainFactory.intRange(1, 5); // {1,2,3,4}
        Domain u2 = DomainFactory.intRange(1, 4); // {1,2,3}
        Domain u3 = DomainFactory.intRange(1, 5); // {1,2,3,4}
        FuzzySet r1 = new MutableFuzzySet(DomainFactory.combine(u1, u2))
                .set(DomainElement.of(1,1), 0.3)
                .set(DomainElement.of(1,2), 1)
                .set(DomainElement.of(3,3), 0.5)
                .set(DomainElement.of(4,3), 0.5);

        FuzzySet r2 = new MutableFuzzySet(DomainFactory.combine(u2, u3))
                .set(DomainElement.of(1,1), 1)
                .set(DomainElement.of(2,1), 0.5)
                .set(DomainElement.of(2,2), 0.7)
                .set(DomainElement.of(3,3), 1)
                .set(DomainElement.of(3,4), 0.4);

        FuzzySet r1r2 = Relations.compositionOfBinaryRelations(r1, r2);
        for(DomainElement e : r1r2.getDomain()) {
            System.out.println("mu("+e+")="+r1r2.getValueAt(e));
        }

        /*
            mu((1,1))=0.5
            mu((1,2))=0.7
            mu((1,3))=0.0
            mu((1,4))=0.0
            mu((2,1))=0.0
            mu((2,2))=0.0
            mu((2,3))=0.0
            mu((2,4))=0.0
            mu((3,1))=0.0
            mu((3,2))=0.0
            mu((3,3))=0.5
            mu((3,4))=0.4
            mu((4,1))=0.0
            mu((4,2))=0.0
            mu((4,3))=0.5
            mu((4,4))=0.4
         */
    }
}
