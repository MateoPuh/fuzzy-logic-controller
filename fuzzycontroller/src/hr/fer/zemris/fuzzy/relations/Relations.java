package hr.fer.zemris.fuzzy.relations;

import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.domain.DomainFactory;
import hr.fer.zemris.fuzzy.set.FuzzySet;
import hr.fer.zemris.fuzzy.set.MutableFuzzySet;

public class Relations {

    private static final double EPSILON = 1e-6;

    public static boolean isUtimesURelation(FuzzySet relation) {
        return relation.getDomain().getNumberOfComponents() == 2 &&
                relation.getDomain().getComponent(0).equals(relation.getDomain().getComponent(1));
    }

    public static boolean isFuzzyEquivalence(FuzzySet relation) {
        return isSymmetric(relation) && isReflexive(relation) && isMaxMinTransitive(relation);
    }

    public static boolean isSymmetric(FuzzySet relation) {
        if (!isUtimesURelation(relation)) return false;

        var domain = relation.getDomain();
        var xDomain = domain.getComponent(0);
        var yDomain = domain.getComponent(1);

        for (DomainElement x : xDomain) {
            for (DomainElement y : yDomain) {
                var xy = DomainElement.of(x.getComponentValue(0), y.getComponentValue(0));
                var yx = DomainElement.of(y.getComponentValue(0), x.getComponentValue(0));

                var uXY = relation.getValueAt(xy);
                var uYX = relation.getValueAt(yx);

                if (Math.abs(uXY - uYX) > EPSILON) {
                    return false;
                }
            }
        }

        return true;
    }

    public static boolean isReflexive(FuzzySet relation) {
        if (!isUtimesURelation(relation)) return false;

        var domain = relation.getDomain();
        var cardinality = domain.getComponent(0).getCardinality();

        for (int i = 0; i < cardinality; i++) {
            var index = i * cardinality + i;
            var element = domain.getComponent(0).elementForIndex(index);
            var uR = relation.getValueAt(element);

            if (Math.abs(uR - 1) > EPSILON) {
                return false;
            }
        }

        return true;
    }

    public static boolean isMaxMinTransitive(FuzzySet relation) {
        if (!isUtimesURelation(relation)) return false;

        var domain = relation.getDomain();
        var xDomain = domain.getComponent(0);

        for (DomainElement x : xDomain) {
            for (DomainElement z : xDomain) {
                var xz = DomainElement.of(x.getComponentValue(0), z.getComponentValue(0));
                var uXZ = relation.getValueAt(xz);

                double max = getMax(x, z, relation, relation);

                if (uXZ < max) {
                    return false;
                }
            }
        }

        return true;
    }

    public static FuzzySet compositionOfBinaryRelations(FuzzySet r1, FuzzySet r2) {
        var u = r1.getDomain().getComponent(0);
        var w = r2.getDomain().getComponent(1);

        var newDomain = DomainFactory.combine(u, w);

        var res = new MutableFuzzySet(newDomain);

        for (DomainElement x : u) {
            for (DomainElement z : w) {
                var xz = DomainElement.of(x.getComponentValue(0), z.getComponentValue(0));
                var max = getMax(x, z, r1, r2);
                res.set(xz, max);
            }
        }

        return res;
    }

    private static double getMax(DomainElement x, DomainElement z, FuzzySet r1, FuzzySet r2) {
        double max = 0;

        var v = r1.getDomain().getComponent(1);

        for (DomainElement y : v) {
            var xy = DomainElement.of(x.getComponentValue(0), y.getComponentValue(0));
            var yz = DomainElement.of(y.getComponentValue(0), z.getComponentValue(0));

            var uA = r1.getValueAt(xy);
            var uB = r2.getValueAt(yz);

            var min = Math.min(uA, uB);

            if (min > max) {
                max = min;
            }
        }

        return max;
    }
}
