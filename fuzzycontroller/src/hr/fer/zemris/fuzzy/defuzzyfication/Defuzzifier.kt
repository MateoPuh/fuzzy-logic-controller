package hr.fer.zemris.fuzzy.defuzzyfication

import hr.fer.zemris.fuzzy.set.FuzzySet

interface Defuzzifier {

    fun defuzzify(fuzzySet: FuzzySet): Int
}
