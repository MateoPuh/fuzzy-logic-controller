package hr.fer.zemris.fuzzy.defuzzyfication

import hr.fer.zemris.fuzzy.set.FuzzySet
import kotlin.math.roundToInt

class COADefuzzifier : Defuzzifier {

    override fun defuzzify(fuzzySet: FuzzySet): Int =
        centerOfAreaDenominator(fuzzySet).let {
            if (it == 0.0) {
                0
            } else {
                (centerOfAreaNumerator(fuzzySet) / centerOfAreaDenominator(fuzzySet)).roundToInt()
            }
        }

    private fun centerOfAreaDenominator(fuzzySet: FuzzySet) =
        fuzzySet.domain
            .map(fuzzySet::getValueAt)
            .reduce(Double::plus)

    private fun centerOfAreaNumerator(fuzzySet: FuzzySet) =
        fuzzySet.domain
            .map { it.getComponentValue(0) * fuzzySet.getValueAt(it) }
            .reduce(Double::plus)
}
