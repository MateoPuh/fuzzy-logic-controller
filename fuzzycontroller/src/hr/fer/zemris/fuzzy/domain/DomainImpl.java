package hr.fer.zemris.fuzzy.domain;

import java.util.Iterator;

public abstract class DomainImpl implements Domain {

    protected DomainImpl() { }

    @Override
    public Iterator<DomainElement> iterator() {
        return new SimpleDomainIterator();
    }

    protected class SimpleDomainIterator implements Iterator<DomainElement> {

        private int current = 0;

        @Override
        public boolean hasNext() {
            return current < getCardinality();
        }

        @Override
        public DomainElement next() {
            current++;
            return elementForIndex(current - 1);
        }
    }
}
