package hr.fer.zemris.fuzzy.domain;

public interface Domain extends Iterable<DomainElement> {

    int getCardinality();

    Domain getComponent(int index);

    int getNumberOfComponents();

    int indexOfElement(DomainElement element);

    DomainElement elementForIndex(int index);
}
