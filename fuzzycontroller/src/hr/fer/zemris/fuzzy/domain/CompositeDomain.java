package hr.fer.zemris.fuzzy.domain;

import java.util.Arrays;

public class CompositeDomain extends DomainImpl {

    private final SimpleDomain[] components;

    public CompositeDomain(SimpleDomain[] components) {
        this.components = components;
    }

    @Override
    public int getCardinality() {
        return Arrays.stream(components).map(Domain::getCardinality).reduce(1, (acc, cardinality) -> acc * cardinality);
    }

    @Override
    public Domain getComponent(int index) {
        return components[index];
    }

    @Override
    public int getNumberOfComponents() {
        return components.length;
    }

    @Override
    public int indexOfElement(DomainElement element) {
        int index = 0;

        for (int i = 0; i < element.getNumberOfComponents() - 1; i++) {
            int nextCardinality = components[i + 1].getCardinality();
            var domainElement = DomainElement.of(element.getComponentValue(i));
            int componentIndex = components[i].indexOfElement(domainElement);

            index += nextCardinality * componentIndex;
        }

        var lastIndex = element.getNumberOfComponents() - 1;
        var domainElement = DomainElement.of(element.getComponentValue(lastIndex));

        index += components[lastIndex].indexOfElement(domainElement);

        return index;
    }

    @Override
    public DomainElement elementForIndex(int index) {
        int[] componentIndex = new int[getNumberOfComponents()];

        for (int i = 0; i < getNumberOfComponents() - 1; i++) {
            var nextCardinality = components[i + 1].getCardinality();
            var position = index / nextCardinality;
            componentIndex[i] = position + components[i].getFirst();

            index = index % nextCardinality;
        }

        var lastIndex = getNumberOfComponents() - 1;
        var position = index;
        componentIndex[lastIndex] = position + components[lastIndex].getFirst();

        return DomainElement.of(componentIndex);
    }
}
