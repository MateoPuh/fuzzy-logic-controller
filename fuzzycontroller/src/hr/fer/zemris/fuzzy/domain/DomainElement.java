package hr.fer.zemris.fuzzy.domain;

import java.util.Arrays;

public class DomainElement {

    private static final int SINGLE_COMPONENT_COUNT = 1;

    private final int[] values;

    private DomainElement(int[] values) {
        this.values = values;
    }

    public static DomainElement of(int... values) {
        return new DomainElement(values);
    }

    public int getNumberOfComponents() {
        return values.length;
    }

    public int getComponentValue(int index) {
        return values[index];
    }

    @Override
    public String toString() {
        return getNumberOfComponents() == SINGLE_COMPONENT_COUNT ? String.valueOf(values[0]) : Arrays.toString(values);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainElement that = (DomainElement) o;
        return Arrays.equals(values, that.values);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(values);
    }
}
