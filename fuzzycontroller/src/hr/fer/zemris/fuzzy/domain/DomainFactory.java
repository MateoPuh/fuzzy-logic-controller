package hr.fer.zemris.fuzzy.domain;

public class DomainFactory {

    public static Domain intRange(int start, int end) {
        return new SimpleDomain(start, end);
    }

    public static Domain combine(Domain... domains) {
        return new CompositeDomain((SimpleDomain[]) domains);
    }
}
