package hr.fer.zemris.fuzzy.domain;

public class SimpleDomain extends DomainImpl {

    private static final int SIMPLE_NUMBER_OF_COMPONENTS = 1;

    private final int first;
    private final int second;

    public SimpleDomain(int first, int second) {
        super();
        this.first = first;
        this.second = second;
    }

    public int getFirst() {
        return first;
    }

    public int getSecond() {
        return second;
    }

    @Override
    public int getCardinality() {
        return second - first;
    }

    @Override
    public Domain getComponent(int index) {
        return this;
    }

    @Override
    public int getNumberOfComponents() {
        return SIMPLE_NUMBER_OF_COMPONENTS;
    }

    @Override
    public int indexOfElement(DomainElement element) {
        return element.getComponentValue(0) - first;
    }

    @Override
    public DomainElement elementForIndex(int index) {
        return DomainElement.of(first + index);
    }

    @Override
    public String toString() {
        StringBuilder domain = new StringBuilder();
        for (DomainElement element : this) {
            domain.append("Element domene: ").append(element).append("\n");
        }

        return domain.toString();
    }
}
